package taller.mundo.teams;

/*
 * MergeSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 * 
 * this method has benn modified with the code find in:
 * http://algs4.cs.princeton.edu/22mergesort/MergeBU.java.html
 */

import java.util.Arrays;
import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class MergeSortTeam extends AlgorithmTeam
{
     public MergeSortTeam()
     {
          super("Merge sort (*)");
          userDefined = true;
     }

     @Override
     public Comparable[] sort(Comparable[] lista, TipoOrdenamiento orden)
     {
          return merge_sort(lista, orden);
     }


     private static Comparable[] merge_sort(Comparable[] lista, TipoOrdenamiento orden)
     {
    	 // Trabajo en Clase
    	 int n = lista.length;
         Comparable[] aux = new Comparable[n];
         for (int len = 1; len < n; len *= 2) {
             for (int lo = 0; lo < n-len; lo += len+len) {
                 int mid  = lo+len-1;
                 int hi = Math.min(lo+len+len-1, n-1);
                 merge(lista, aux, lo, mid, hi, orden);
             }
         }
    	 
    	 return lista;
     }

     private static Comparable[] merge(Comparable[] izquierda, Comparable[] derecha, int lo, int mid, int hi, TipoOrdenamiento orden)
     {
    	// Trabajo en Clase 
    	if (orden.equals(TipoOrdenamiento.ASCENDENTE))
    	{
    	 // copy to aux[]
         for (int k = lo; k <= hi; k++) {
             derecha[k] = izquierda[k]; 
         }

         // merge back to a[]
         int i = lo, j = mid+1;
         for (int k = lo; k <= hi; k++) {
             if      (i > mid)              izquierda[k] = derecha[j++];  // this copying is unneccessary
             else if (j > hi)               izquierda[k] = derecha[i++];
             else if (izquierda[j].compareTo(izquierda[i]) < 0 ) izquierda[k] = derecha[j++];
             else                           izquierda[k] = derecha[i++];
         }
    	}
    	else 
    	{
    		// copy to aux[]
            for (int k = lo; k <= hi; k++) {
                derecha[k] = izquierda[k]; 
            }

            // merge back to a[]
            int i = lo, j = mid+1;
            for (int k = lo; k <= hi; k++) {
                if      (i > mid)              izquierda[k] = derecha[j++];  // this copying is unneccessary
                else if (j > hi)               izquierda[k] = derecha[i++];
                else if (izquierda[j].compareTo(izquierda[i]) < 0 ) izquierda[k] = derecha[j++];
                else                           izquierda[k] = derecha[i++];
            }
    	}
    	return izquierda;    	
     }


}
