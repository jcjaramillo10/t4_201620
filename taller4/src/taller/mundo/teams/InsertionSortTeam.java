package taller.mundo.teams;

/*
 * InsertionSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 * 
 * This class has been modified with the method established in:
 * http://quiz.geeksforgeeks.org/insertion-sort/
 */

import java.util.Arrays;
import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class InsertionSortTeam extends AlgorithmTeam
{
     public InsertionSortTeam()
     {
          super("Insertion sort (-)");
          userDefined = false;
     }

     @Override
     public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden)
     {
          return insertionSort(list, orden);
     }

     /*
     Ordena un arreglo de enteros, usando Ordenamiento por inserci�n.
     @param arr Arreglo de enteros.
   **/
   private Comparable[] insertionSort(Comparable[] arr, TipoOrdenamiento orden)
   {
	    // Para hacer en casa
	   if (orden.equals(TipoOrdenamiento.DESCENDENTE))
	   {
		   int n = arr.length;
		   for (int i=1; i<n; ++i)
		   {
			   Comparable key = arr[i];
			   int j = i-1;

			   /* Move elements of arr[0..i-1], that are
              greater than key, to one position ahead
              of their current position */
			   while (j>=0 && arr[j].compareTo(key) > 0)
			   {
				   arr[j+1] = arr[j];
				   j = j-1;
			   }
			   arr[j+1] = key;
		   }
		   
	   }
	   else
	   {
		   int n = arr.length;
		   for (int i=1; i<n; ++i)
		   {
			   Comparable key = arr[i];
			   int j = i-1;

			   /* Move elements of arr[0..i-1], that are
	              greater than key, to one position ahead
	              of their current position */
			   while (j>=0 && arr[j].compareTo(key) < 0)
			   {
				   arr[j+1] = arr[j];
				   j = j-1;
			   }
			   arr[j+1] = key;
		   }
		   
	   }
	   return arr;
   }

   private boolean getState(Comparable a, Comparable b, TipoOrdenamiento orden)
   {
         int stat = a.compareTo(b);
         boolean val = stat > 0 && orden == TipoOrdenamiento.ASCENDENTE;
         val = val || stat < 0 && orden == TipoOrdenamiento.DESCENDENTE;
         return val;
   }
}
